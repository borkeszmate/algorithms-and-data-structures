class Node {
	constructor(data, next = null ) {
		this.data = data;
		this.next = next;
	}
}

class LinkedList {
	constructor() {
		this.head = null;
	}

	insert(data) {
		const node = new Node(data, this.head);
		this.head = node;
	}

	size() {
		let counter = 0;
		let node = this.head;
		while(node) {
			counter++;
			node = node.next;
		}
	}
}
