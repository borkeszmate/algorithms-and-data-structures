class Stack {
	constructor() {
		this.data =[];
	}

	add(node) {
		this.data.push(node);
	}

	remove() {
		return this.data.pop();
	}

	peek() {
		return this.data[this.data.length - 1];
	}
}

module.exports = Stack;
