
class Node {
	constructor(data, next = null) {
		this.data = data;
		this.next = next;
	}

}

class LinkedList {
	constructor() {
		this.head = null;
	}

	insertNode(data) {
		this.head = new Node(data, this.head);
		console.log(this.head);
	}

	size() {
		let counter = 0;
		let node = this.head;
		while (node) {
			counter++;
			node = node.next;
		}

		return counter;
	}

	getFirst() {
		return this.head;
	}

	getTail(current= this.head) {
		if (this.head === null) {
			return null;
		}
		if (current.next !== null) {
			return this.getTail(current.next);
		}
		return current;
	}
}

const list = new LinkedList();
list.insertNode('első');



module.exports = { Node, LinkedList }


